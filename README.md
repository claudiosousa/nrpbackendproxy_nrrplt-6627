This repository is part of the Neurorobotics Platform software
Copyright (C) Human Brain Project
https://neurorobotics.ai

The Human Brain Project is a European Commission funded project
in the frame of the Horizon2020 FET Flagship plan.
http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships

You are free to clone this repository and amend its code with respect to
the license that you find in the root folder.

## Submitting a pull request

To submit code changes (pull requests), do as follows. 

0. Log in to Bitbucket
1. [Fork the project](https://bitbucket.org/hbpneurorobotics/nrpbackendproxy/fork)
2. Clone the forked project (eg: ```git clone git@bitbucket.org:[USERNAME]/[REPOSITORY].git```)
3. Do your code changes
4. Commit your changes. \
  **Make sure** your commit message starts with [<ticket_number>].
   Example: "[NUIT-10] My new feature"
5. ```git push```
6. Click on the url provided on the console output for the previous command to create the pull request
7. A core developer will eventually review your pull request and approve or comment it
